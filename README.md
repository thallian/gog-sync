# GOG-SYNC
## **Please note that this is alpha software, you should not trust it.**

A small tool to synchronize the stuff in a [GOG](https://www.gog.com/) library
with a local folder.

It builds on the work of the [unofficial GOG API Documentation](https://gogapidocs.readthedocs.io/en/latest/).

This is the first time I am building something with rust, so beware :)

# Installation
Install from [crates.io](https://crates.io/crates/gog-sync).

```
cargo install gog-sync
```

# Configuration
The configuration file is in the config folder as described by the xdg specification
with a prefix of `gog-sync`.

For example on macOS or Linux

```
~/.config/gog-sync/config.json
```

A bare configuration with default values before first use:

```
{
  "gameStorage": ".",
  "movieStorage": ".",
  "osFilters": [],
  "languageFilters": [],
  "resolutionFilters": [],
  "skipMovies": false,
  "skipGames": false
}
```

- *gameStorage*: Where to save games
- *movieStorage*: Where to save movies
- *osFilters*: An array of operating systems. If it is not empty, game data is limited to the ones in the list.
- *languageFilters*: An array of languages. If it is not empty, game data is limited to the ones in the list.
- *resolutionFilters*: An array of resolutions. If it is not empty, movie data is limited to the ones in the list.
- *skipMovies*: Whether to skip movie content
- *skipGames*: Whether to skip game content

Valid values for *osFilter*:
- `linux`
- `mac`
- `windows`

Check on [gog.com](https://www.gog.com/) which languages are available.

An incomplete list of resolutions on gog:
- `DVD`
- `576p`
- `720p`
- `1080p`
- `4k`

# Usage

If you want to see the information log while running set `RUST_LOG=info`.

```
USAGE:
    gog-sync [FLAGS] [OPTIONS]

FLAGS:
    -h, --help           Prints help information
    -g, --skip-games     Skip game content.
    -f, --skip-movies    Skip movie content.
    -V, --version        Prints version information

OPTIONS:
    -s, --game-storage <FOLDER>     Sets the download folder (defaults to the working directory).
    -l, --language <FILTER>         Only sync files for this comma seperated list of languages.
    -m, --movie-storage <FOLDER>    Sets the download folder for movies (defaults to the working directory).
    -o, --os <FILTER>               Only sync files for this comma seperated list of operating systems.
                                    Valid values are 'linux', 'mac'  and 'windows'.
    -r, --resolution <FILTER>       Only sync movies for this comma seperated list of resolutions.
```

---

```
gog-sync
```

Normal invocation, uses the current working directory as storage if not configured
otherwise.

---

```
gog-sync -s ~/Downloads/games
```

Overwrite the default or configured storage path.

---

```
gog-sync -l english -o linux,windows
```

Only sync english installers and only for linux and windows systems.
