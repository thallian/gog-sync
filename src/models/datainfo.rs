#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DataInfo {
    pub hash: u64,
    pub filename: String,
    pub language: String,
}
