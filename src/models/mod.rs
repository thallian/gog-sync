use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;

pub mod config;
pub mod content;
pub mod contentinfo;
pub mod data;
pub mod datainfo;
pub mod extra;
pub mod extrainfo;
pub mod token;

pub fn get_hash<T>(obj: &T) -> u64
    where T: Hash
{
    let mut s = DefaultHasher::new();
    obj.hash(&mut s);
    s.finish()
}
