#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Config {
    #[serde(default)]
    pub game_storage: String,
    #[serde(default)]
    pub movie_storage: String,
    #[serde(default = "default_list")]
    pub os_filters: Vec<String>,
    #[serde(default = "default_list")]
    pub language_filters: Vec<String>,
    #[serde(default = "default_list")]
    pub resolution_filters: Vec<String>,
    #[serde(default)]
    pub skip_movies: bool,
    #[serde(default)]
    pub skip_games: bool,
}

impl Config {
    pub fn new() -> Config {
        Config {
            game_storage: String::from("."),
            movie_storage: String::from("."),
            os_filters: Vec::new(),
            language_filters: Vec::new(),
            resolution_filters: Vec::new(),
            skip_movies: false,
            skip_games: false,
        }
    }
}

fn default_list() -> Vec<String> {
    Vec::new()
}
