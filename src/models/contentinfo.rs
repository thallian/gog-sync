use models::datainfo::DataInfo;
use models::extrainfo::ExtraInfo;
use std::collections::BTreeMap;
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ContentInfo {
    #[serde(skip_deserializing)]
    pub id: u64,
    pub hash: u64,
    pub title: String,
    pub cd_keys: BTreeMap<String, String>,
    pub data: HashMap<String, DataInfo>,
    pub extras: HashMap<String, ExtraInfo>,
}

impl ContentInfo {
    pub fn new() -> ContentInfo {
        ContentInfo {
            id: 0,
            hash: 0,
            title: String::new(),
            cd_keys: BTreeMap::new(),
            data: HashMap::new(),
            extras: HashMap::new(),
        }
    }
}
