use std::fmt;

#[derive(Hash)]
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Data {
    pub manual_url: String,
    pub version: String,
    pub os: String,
    pub language: String,
}

impl fmt::Display for Data {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "({}, {}, {}, {})",
               self.manual_url,
               self.version,
               self.os,
               self.language)
    }
}
