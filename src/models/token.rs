use chrono::UTC;

#[derive(Serialize, Deserialize)]
pub struct Token {
    pub access_token: String,
    pub expires_in: u16,
    pub token_type: String,
    pub scope: String,
    pub session_id: String,
    pub refresh_token: String,
    pub user_id: String,
    #[serde(default = "timestamp")]
    pub timestamp: i64,
}

impl Token {
    pub fn is_expired(&self) -> bool {
        let now = UTC::now().timestamp();

        now > self.timestamp + self.expires_in as i64
    }
}

fn timestamp() -> i64 {
    UTC::now().timestamp()
}
