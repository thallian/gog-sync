#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExtraInfo {
    pub hash: u64,
    pub filename: String,
    pub name: String,
}
