use std::fmt;

#[derive(Hash)]
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Extra {
    pub manual_url: String,
    pub name: String,
}

impl fmt::Display for Extra {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.manual_url, self.name)
    }
}
