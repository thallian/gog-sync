## 0.3.4 (2017-05-10)
- sync dlcs (fixes #12)
- break content deserializing and sync code into smaller pieces (fixes #9)

## 0.3.3 (2017-05-09)
- normalize content directory names (fixes #11)

## 0.3.2 (2017-05-05)
- check for invalid grant when fetching content too (really fixes #7)

## 0.3.1 (2017-05-05)
- automatically check if grant is still valid and refresh if necessary (fixes #7)

## 0.3.0 (2017-05-04)
- move hashes and content info into seperate files as to not clutter up the main config file

## 0.2.4 (2017-03-23)
- fix a bug in serial key parsing

## 0.2.3 (2017-03-23)
- correctly parse serial keys (closes #8)

## 0.2.2 (2017-03-22)
- ability to filter movies by resolution
- ability to save movies seperately
- default value for storage config (fixes #6)
- ability to skip movie and game content
- fix a bug with truncating an http response

## 0.2.1 (2017-03-21)
- add instructions for getting the code if there is no token

## 0.2.0 (2017-03-21)
- add filtering support for operating systems and languages

## 0.1.0 (2017-03-21)
- initial release
